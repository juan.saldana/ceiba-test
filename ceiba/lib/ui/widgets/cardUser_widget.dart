import 'package:ceiba/ui/pages/home/home_page.dart';
import 'package:ceiba/ui/pages/publications/publications_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/ui.dart';

import '../../routes/routes.dart';

class CardUser extends StatelessWidget {
  const CardUser({
    Key? key,
    required double width,
    required String name,
    required String phone,
    required String email,
    required String website,
    required String nameCompany,
    required String catchPhrase,
    required Map<String, dynamic> address,
    required int id,
  })  : _width = width,
        _name = name,
        _phone = phone,
        _email = email,
        _website = website,
        _nameCompany = nameCompany,
        _catchPhrase = catchPhrase,
        _address = address,
        _id = id,
        super(key: key);

  final double _width;
  final String _name;
  final String _phone;
  final String _email;
  final String _website;
  final String _nameCompany;
  final String _catchPhrase;
  final Map<String, dynamic> _address;
  final int _id;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        await publicationsProvider.read.chargeData(_id.toString(), _name,
            _phone, _email, _website, _nameCompany, _catchPhrase, _address);
        Navigator.pushNamed(context, publications);
      },
      child: Card(
        elevation: 8,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: _width * 0.02),
              width: _width * 0.8,
              child: Text(_name),
            ),
            row(_phone, Icons.phone),
            row(_email, Icons.email),
            Consumer(
              builder: ((context, ref, child) {
                final controllerHome = ref.watch(homeProvider);
                return TextButton(
                  onPressed: () async {
                    controllerHome.updateIsLoading(true);
                    await publicationsProvider.read.chargeData(
                        _id.toString(),
                        _name,
                        _phone,
                        _email,
                        _website,
                        _nameCompany,
                        _catchPhrase,
                        _address);
                    controllerHome.updateIsLoading(false);
                    Navigator.pushNamed(context, publications);
                  },
                  child: SizedBox(
                    width: _width,
                    child: const Text(
                      'SEE POSTS',
                      textAlign: TextAlign.right,
                    ),
                  ),
                );
              }),
            )
          ],
        ),
      ),
    );
  }

  SizedBox row(String text, IconData icon) {
    return SizedBox(
        width: _width * 0.8,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(icon),
            SizedBox(
              width: _width * 0.03,
            ),
            Text(text),
          ],
        ));
  }
}
