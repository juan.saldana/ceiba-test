import 'package:ceiba/ui/pages/publications/controller/publications_controller.dart';
import 'package:ceiba/ui/pages/publications/publications_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Publication Controller', () {
    test('init variables', () {
      final controller = PublicationsController();
      expect(controller.listPosts, []);
      expect(controller.isLoading, false);
      expect(controller.name, '');
      expect(controller.email, '');
      expect(controller.phone, '');
      expect(controller.website, '');
      expect(controller.nameCompany, '');
      expect(controller.catchPhrase, '');
      expect(controller.address, {'suite': '', 'city': '', 'street': ''});
      expect(controller.isPage, false);
    });
  });

  group('Publication Page', () {
    testWidgets('rendering publication page and iconbuttons', (tester) async {
      await tester.pumpWidget(const MaterialApp(
        home: PublicationsPage(),
      ));

      expect(find.widgetWithIcon(Container, Icons.code), findsOneWidget);
      await tester.tap(find.widgetWithIcon(IconButton, Icons.code));
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) => widget is PageView),
          findsOneWidget);

      expect(find.widgetWithIcon(Container, Icons.format_list_bulleted),
          findsOneWidget);
      await tester.tap(
        find.widgetWithIcon(IconButton, Icons.format_list_bulleted),
      );
      await tester.pumpAndSettle();
      expect(find.byWidgetPredicate((widget) => widget is ListView),
          findsOneWidget);
    });
  });
}
