import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';

import '../../../../data/data_sources/remote/services_api.dart';

class PublicationsController extends SimpleNotifier {
  final _repository = ServicesAPI();

  List<dynamic> _listPosts = [];
  List<dynamic> get listPosts => _listPosts;
  void updateListPosts(List<dynamic> posts) {
    _listPosts = posts;
    notify();
  }

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  void updateIsLoading(bool value) {
    _isLoading = value;
    notify();
  }

  String _name = '';
  String get name => _name;
  String _email = '';
  String get email => _email;
  String _phone = '';
  String get phone => _phone;
  String _website = '';
  String get website => _website;
  String _nameCompany = '';
  String get nameCompany => _nameCompany;
  String _catchPhrase = '';
  String get catchPhrase => _catchPhrase;
  Map<String, dynamic> _address = {'suite': '', 'city': '', 'street': ''};
  Map<String, dynamic> get address => _address;

  bool _isPage = false;
  bool get isPage => _isPage;

  void switchList() {
    if (_isPage) {
      _isPage = false;
    } else {
      _isPage = true;
    }
    notify();
  }

  Future<List<dynamic>> postsForId(String id) async {
    final response = await _repository.postsForId(id);
    notify();
    return response!;
  }

  Future<void> chargeData(
      String id,
      String name,
      String phone,
      String email,
      String website,
      String nameCompany,
      String catchPhrase,
      Map<String, dynamic> address) async {
    final response = await postsForId(id);
    updateListPosts(response);
    _name = name;
    _email = email;
    _phone = phone;
    _website = website;
    _nameCompany = nameCompany;
    _catchPhrase = catchPhrase;
    _address = address;
    notify();
  }

  Row rowDataDuble(double _width, IconData icon1, String text1, IconData icon2,
      String text2) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        rowData(_width, icon1, text1),
        rowData(_width, icon2, text2),
      ],
    );
  }

  Row rowData(double _width, IconData icon, String text) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(
          icon,
          color: const Color.fromRGBO(102, 177, 118, 1),
          size: _width * 0.08,
        ),
        SizedBox(
          width: _width * 0.02,
        ),
        Text(
          text,
          style: TextStyle(fontSize: _width * 0.03),
        ),
      ],
    );
  }
}
