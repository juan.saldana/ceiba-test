import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:flutter_meedu/ui.dart';

import '../../widgets/cardposts_widget.dart';
import '../../widgets/loading_widget.dart';
import 'controller/publications_controller.dart';

final publicationsProvider = SimpleProvider((_) => PublicationsController());

class PublicationsPage extends StatelessWidget {
  const PublicationsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height -
        MediaQuery.of(context).size.height * 0.15;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Posts'),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Consumer(
              builder: (context, ref, child) {
                final controllerPublications = ref.watch(publicationsProvider);
                final listPosts = controllerPublications.listPosts;
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: _height * 0.02),
                        child: Text(
                          controllerPublications.name,
                          style: TextStyle(
                              fontSize: _width * 0.08,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: _height * 0.02),
                        child: controllerPublications.rowDataDuble(
                            _width,
                            Icons.email,
                            controllerPublications.email,
                            Icons.phone,
                            controllerPublications.phone),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: _height * 0.02),
                        child: controllerPublications.rowDataDuble(
                            _width,
                            Icons.language,
                            controllerPublications.website,
                            Icons.business,
                            controllerPublications.nameCompany),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: _height * 0.02),
                        child: controllerPublications.rowData(
                            _width,
                            Icons.place,
                            controllerPublications.address['street'] +
                                ', ' +
                                controllerPublications.address['suite'] +
                                ', ' +
                                controllerPublications.address['city']),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: _height * 0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            IconButton(
                              onPressed: () {
                                controllerPublications.switchList();
                              },
                              icon: Icon(
                                Icons.format_list_bulleted,
                                color: !controllerPublications.isPage
                                    ? const Color.fromRGBO(43, 98, 54, 1)
                                    : Colors.black87,
                              ),
                            ),
                            IconButton(
                              onPressed: () {
                                controllerPublications.switchList();
                              },
                              icon: Icon(
                                Icons.code,
                                color: controllerPublications.isPage
                                    ? const Color.fromRGBO(43, 98, 54, 1)
                                    : Colors.grey,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: _height * 0.56,
                        width: _width,
                        child: controllerPublications.isPage
                            ? PageView.builder(
                                itemBuilder: (context, index) {
                                  return CardPost(
                                    height: _height,
                                    title: listPosts[index]['title'],
                                    body: listPosts[index]['body'],
                                    id: listPosts[index]['id'].toString(),
                                  );
                                },
                                itemCount: listPosts.length,
                              )
                            : ListView.builder(
                                itemBuilder: (context, index) {
                                  return CardPost(
                                    height: _height,
                                    title: listPosts[index]['title'],
                                    body: listPosts[index]['body'],
                                    id: listPosts[index]['id'].toString(),
                                  );
                                },
                                itemCount: listPosts.length,
                              ),
                      ),
                    ],
                  ),
                );
              },
            ),
            Consumer(
              builder: (_, ref, __) {
                final controllerConsumer = ref.watch(publicationsProvider);
                return controllerConsumer.isLoading
                    ? const Loading()
                    : Container();
              },
            ),
          ],
        ),
      ),
    );
  }
}
