import '../../domain/repositories/users_repository.dart';
import '../data_sources/local/users_client.dart';

class UsersRepositoryImpl implements UsersRepository {
  final UsersClient _client;
  UsersRepositoryImpl(this._client);

  @override
  Future<String?> get accessUsers => _client.accessUsers;

  @override
  Future<void> setUsers(String? users) {
    return _client.setUsers(users.toString());
  }

  @override
  Future<void> clear() {
    return _client.clear();
  }
}
