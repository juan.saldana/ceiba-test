import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';

import '../../../../data/data_sources/remote/services_api.dart';
import '../../../../domain/repositories/users_repository.dart';

class HomeController extends SimpleNotifier {
  final _repository = ServicesAPI();
  final _usersRepository = Get.find<UsersRepository>();

  final TextEditingController _controllerSearch = TextEditingController();
  TextEditingController get controllerSearch => _controllerSearch;

  void reset() {
    _controllerSearch.text = '';
    notify();
  }

  final String _logo =
      'https://www.ceiba.com.co/wp-content/uploads/2021/05/logoCeibaSoftware-horizontal-1.png';
  String get logo => _logo;

  List<dynamic> _listUsers = [];
  List<dynamic> get listUsers => _listUsers;
  void updateListUsers(List<dynamic> users) {
    _listUsers = users;
    notify();
  }

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  void updateIsLoading(bool value) {
    _isLoading = value;
    notify();
  }

  bool _isOk = false;
  bool get isOk => _isOk;

  Future<List<dynamic>> users() async {
    final response = await _repository.users();
    notify();
    return response!;
  }

  Future<void> init() async {
    final usersString = await _usersRepository.accessUsers;
    if (usersString != null) {
      final usersR = jsonDecode(usersString);
      updateListUsers(usersR);
    }
    if (_listUsers.isNotEmpty) {
      _isOk = true;
    } else {
      _isOk = false;
    }
    notify();
  }

  Future<void> chargeData() async {
    final usersString = await users();
    await _usersRepository.setUsers(jsonEncode(usersString));
    updateListUsers(usersString);
    _isOk = true;
    notify();
  }

  Future<void> deleteData() async {
    await _usersRepository.clear();
    _isOk = false;
    notify();
  }

  Future<void> searching(String text) async {
    await init();
    final filter = listUsers
        .where((element) =>
            element['username'].toLowerCase().contains(text.toLowerCase())
                ? true
                : false)
        .toList();
    updateListUsers(filter);
    notify();
  }
}
