abstract class UsersRepository {
  Future<String?> get accessUsers;
  Future<void> setUsers(String? users);
  Future<void> clear();
}
