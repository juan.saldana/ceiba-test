import 'package:flutter_meedu/meedu.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'data/data_sources/local/users_client.dart';
import 'data/repositories_impl/users_repository_impl.dart';
import 'domain/repositories/users_repository.dart';

void injectDependencies() {
  Get.lazyPut<UsersRepository>(() => UsersRepositoryImpl(
        UsersClient(const FlutterSecureStorage()),
      ));
}
