import 'package:flutter_meedu/meedu.dart';

import '../../../../data/data_sources/remote/services_api.dart';

class CommentsController extends SimpleNotifier {
  final _repository = ServicesAPI();

  List<dynamic> _listComments = [];
  List<dynamic> get listComments => _listComments;
  void updateListComments(List<dynamic> comments) {
    _listComments = comments;
    notify();
  }

  String _title = '';
  String get title => _title;
  String _body = '';
  String get body => _body;

  Future<List<dynamic>> commentsForId(String id) async {
    final response = await _repository.commentsForId(id);
    notify();
    return response!;
  }

  Future<void> chargeData(String id, String title, String body) async {
    final response = await commentsForId(id);
    updateListComments(response);
    _title = title;
    _body = body;
    notify();
  }
}
