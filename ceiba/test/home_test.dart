import 'package:ceiba/data/data_sources/local/users_client.dart';
import 'package:ceiba/data/repositories_impl/users_repository_impl.dart';
import 'package:ceiba/domain/repositories/users_repository.dart';
import 'package:ceiba/ui/pages/home/controller/home_controller.dart';
import 'package:ceiba/ui/pages/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:network_image_mock/network_image_mock.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  setUpAll(() async {
    const FlutterSecureStorage storage = FlutterSecureStorage();
    final UsersClient client = UsersClient(storage);
    Get.put<UsersRepository>(UsersRepositoryImpl(client));
  });
  group('Home Controller', () {
    test('init variables', () {
      final controller = HomeController();
      expect(controller.isLoading, false);
      expect(controller.isOk, false);
      expect(controller.logo.contains('ceiba'), true);
      expect(controller.listUsers, []);
    });
  });

  group('Home Page', () {
    testWidgets('rendering home page and iconbutton off ang logo',
        (tester) async {
      final controller = HomeController();

      mockNetworkImagesFor(() async {
        await tester.pumpWidget(const MaterialApp(
          home: HomePage(),
        ));

        expect(find.widgetWithIcon(Scaffold, Icons.power_settings_new_rounded),
            findsOneWidget);

        await tester.tap(
            find.widgetWithIcon(IconButton, Icons.power_settings_new_rounded));
        await tester.pumpAndSettle();

        expect(find.widgetWithImage(Container, NetworkImage(controller.logo)),
            findsOneWidget);
      });
    });
  });
}
