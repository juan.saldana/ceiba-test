import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:flutter_meedu/ui.dart';

import 'controller/splash_controller.dart';

final splashProvider = SimpleProvider(
  (_) => SplashController(),
);

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ProviderListener(
      provider: splashProvider,
      onChange: (_, SplashController controller) {
        if (controller.routeName != null) {
          Navigator.pushNamedAndRemoveUntil(
              context, controller.routeName!, (_) => false);
        }
      },
      builder: (_, __) => const Scaffold(
        body: Center(
          child: CupertinoActivityIndicator(
            radius: 20.0,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}
