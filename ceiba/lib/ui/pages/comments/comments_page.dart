import 'package:ceiba/ui/widgets/cardposts_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';

import 'controller/comments_controller.dart';

final commentsProvider = SimpleProvider((_) => CommentsController());

class CommentsPage extends StatelessWidget {
  const CommentsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _height = MediaQuery.of(context).size.height -
        MediaQuery.of(context).size.height * 0.15;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Comments'),
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              height: _height * 0.3,
              margin: EdgeInsets.only(top: _height * 0.01),
              child: CardPost(
                  height: _height,
                  title: commentsProvider.read.title,
                  body: commentsProvider.read.body,
                  id: ''),
            ),
            Container(
              margin: EdgeInsets.only(top: _height * 0.03),
              height: _height * 0.65,
              child: ListView.builder(
                itemBuilder: (context, index) {
                  final listComments = commentsProvider.read.listComments;
                  return Card(
                    elevation: 5,
                    child: ListTile(
                      style: ListTileStyle.drawer,
                      leading: const Icon(Icons.comment_sharp),
                      title: Text(listComments[index]['name']),
                      subtitle: Text(listComments[index]['body']),
                    ),
                  );
                },
                itemCount: commentsProvider.read.listComments.length,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
