// ignore_for_file: avoid_print

import 'package:dio/dio.dart';

class ServicesAPI {
  final _dio =
      Dio(BaseOptions(baseUrl: 'https://jsonplaceholder.typicode.com'));

  Future<List<dynamic>?> users() async {
    try {
      final response = await _dio.get(
        '/users',
      );
      return response.data;
    } on DioError catch (e) {
      print('error users');
      print(e);
      return null;
    }
  }

  Future<List<dynamic>?> postsForId(String id) async {
    try {
      final response =
          await _dio.get('/posts', queryParameters: {'userId': id});
      return response.data;
    } on DioError catch (e) {
      print('error posts for id');
      print(e);
      return null;
    }
  }

  Future<List<dynamic>?> commentsForId(String id) async {
    try {
      final response =
          await _dio.get('/comments', queryParameters: {'postId': id});
      return response.data;
    } on DioError catch (e) {
      print('error posts for id');
      print(e);
      return null;
    }
  }
}
