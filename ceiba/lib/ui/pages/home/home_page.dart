import 'package:flutter/material.dart';
import 'package:flutter_meedu/meedu.dart';
import 'package:flutter_meedu/ui.dart';

import '../../widgets/carduser_widget.dart';
import '../../widgets/loading_widget.dart';
import 'controller/home_controller.dart';

final homeProvider =
    SimpleProvider((_) => HomeController(), autoDispose: false);

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _width = MediaQuery.of(context).size.width;
    final _height = MediaQuery.of(context).size.height -
        MediaQuery.of(context).size.height * 0.15;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Ceiba Test'),
        actions: [
          IconButton(
            onPressed: () async {
              await homeProvider.read.deleteData();
            },
            icon: const Icon(
              Icons.power_settings_new_rounded,
            ),
          )
        ],
      ),
      body: Stack(
        children: [
          Center(child: Consumer(
            builder: (context, ref, child) {
              final controllerHome = ref.watch(homeProvider);
              return Column(
                mainAxisAlignment: controllerHome.isOk
                    ? MainAxisAlignment.spaceAround
                    : MainAxisAlignment.center,
                children: [
                  controllerHome.isOk
                      ? Container(
                          margin: EdgeInsets.only(
                            top: _height * 0.02,
                          ),
                          padding: EdgeInsets.symmetric(
                            horizontal: _height * 0.02,
                          ),
                          child: TextField(
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: 'Search user',
                            ),
                            onChanged: controllerHome.searching,
                          ),
                        )
                      : Container(),
                  controllerHome.isOk
                      ? SizedBox(
                          width: _width * 0.9,
                          height: _height * 0.85,
                          child: ListView.builder(
                            itemBuilder: (context, index) {
                              final listUsers = controllerHome.listUsers;
                              if (listUsers.isNotEmpty) {
                                return SizedBox(
                                  height: _height * 0.2,
                                  child: CardUser(
                                    width: _width,
                                    name: listUsers[index]['username'],
                                    email: listUsers[index]['email'],
                                    phone: listUsers[index]['phone'],
                                    id: listUsers[index]['id'],
                                    website: listUsers[index]['website'],
                                    nameCompany: listUsers[index]['company']
                                        ['name'],
                                    catchPhrase: listUsers[index]['company']
                                        ['catchPhrase'],
                                    address: listUsers[index]['address'],
                                  ),
                                );
                              } else {
                                return const Center(
                                  child: Text(
                                    'List is empty',
                                  ),
                                );
                              }
                            },
                            itemCount: controllerHome.listUsers.isNotEmpty
                                ? controllerHome.listUsers.length
                                : 1,
                          ),
                        )
                      : Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: _height * 0.04),
                          child: GestureDetector(
                            onTap: () async {
                              controllerHome.updateIsLoading(true);
                              await controllerHome.chargeData();
                              controllerHome.updateIsLoading(false);
                            },
                            child: Image.network(controllerHome.logo),
                          ),
                        ),
                ],
              );
            },
          )),
          Consumer(
            builder: (_, ref, __) {
              final controllerConsumer = ref.watch(homeProvider);
              return controllerConsumer.isLoading
                  ? const Loading()
                  : Container();
            },
          ),
        ],
      ),
    );
  }
}
