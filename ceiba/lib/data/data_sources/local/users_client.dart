// ignore_for_file: constant_identifier_names

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

const String USERS = "users";

class UsersClient {
  final FlutterSecureStorage _storage;
  UsersClient(this._storage);

  Future<String?> get accessUsers {
    return _storage.read(key: USERS);
  }

  Future<void> setUsers(String users) {
    return _storage.write(key: USERS, value: users);
  }

  Future<void> clear() {
    return _storage.deleteAll();
  }
}
