import 'package:ceiba/routes/routes.dart';
import 'package:ceiba/ui/pages/comments/comments_page.dart';
import 'package:ceiba/ui/pages/publications/publications_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/ui.dart';

class CardPost extends StatelessWidget {
  const CardPost({
    Key? key,
    required double height,
    required String title,
    required String body,
    required String id,
  })  : _height = height,
        _title = title,
        _body = body,
        _id = id,
        super(key: key);

  final double _height;
  final String _title;
  final String _body;
  final String _id;

  @override
  Widget build(BuildContext context) {
    Color green = const Color.fromRGBO(165, 213, 175, 1);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: _height * 0.02),
      child: Card(
        elevation: 8,
        shadowColor: green,
        shape: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: BorderSide(
            color: green,
            width: 1,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: _height * 0.1,
              decoration: BoxDecoration(
                color: green,
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(10.0),
                  topRight: Radius.circular(10.0),
                ),
              ),
              child: Center(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: _height * 0.01),
                  child: Text(
                    _title,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: _height * 0.02),
              child: Text(
                _body,
                textAlign: TextAlign.center,
              ),
            ),
            _id != ''
                ? SizedBox(
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Consumer(
                        builder: ((context, ref, child) {
                          final controllerPublication =
                              ref.watch(publicationsProvider);
                          return IconButton(
                            onPressed: () async {
                              controllerPublication.updateIsLoading(true);
                              await commentsProvider.read
                                  .chargeData(_id, _title, _body);
                              controllerPublication.updateIsLoading(false);

                              Navigator.pushNamed(context, comments);
                            },
                            icon: const Icon(
                              Icons.comment,
                              color: Color.fromRGBO(43, 98, 54, 1),
                            ),
                          );
                        }),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
