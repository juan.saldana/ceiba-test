import 'package:ceiba/routes/routes.dart';
import 'package:ceiba/ui/pages/comments/comments_page.dart';
import 'package:ceiba/ui/pages/home/home_page.dart';
import 'package:ceiba/ui/pages/publications/publications_page.dart';
import 'package:ceiba/ui/pages/splash/splash_page.dart';
import 'package:flutter/material.dart';

Map<String, Widget Function(BuildContext)> appRoutes = {
  home: (_) => const HomePage(),
  publications: (_) => const PublicationsPage(),
  comments: (_) => const CommentsPage(),
  splash: (_) => const SplashPage(),
};
