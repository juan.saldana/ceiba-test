import 'package:ceiba/routes/pages.dart';
import 'package:ceiba/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_meedu/ui.dart';

import 'dependency_injection.dart';

void main() {
  injectDependencies();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = ThemeData();
    return MaterialApp(
      theme: theme.copyWith(
        colorScheme: theme.colorScheme.copyWith(
          primary: const Color.fromRGBO(43, 98, 54, 1),
        ),
      ),
      navigatorKey: router.navigatorKey,
      navigatorObservers: [router.observer],
      debugShowCheckedModeBanner: false,
      routes: appRoutes,
      initialRoute: splash,
    );
  }
}
