import 'package:ceiba/ui/pages/comments/comments_page.dart';
import 'package:ceiba/ui/pages/comments/controller/comments_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('Comments Controller', () {
    test('init variables', () {
      final controller = CommentsController();
      expect(controller.listComments, []);
      expect(controller.title, '');
      expect(controller.body, '');
    });
  });

  group('Comments Page', () {
    testWidgets('rendering comments page', (tester) async {
      await tester.pumpWidget(const MaterialApp(
        home: CommentsPage(),
      ));

      expect(
          find.byWidgetPredicate((widget) => widget is Card), findsOneWidget);
    });
  });
}
