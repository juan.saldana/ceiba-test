// ignore_for_file: prefer_final_fields, prefer_typing_uninitialized_variables
import 'package:ceiba/routes/routes.dart';
import 'package:ceiba/ui/pages/home/home_page.dart';
import 'package:flutter_meedu/meedu.dart';

class SplashController extends SimpleNotifier {
  SplashController() {
    _init();
  }

  String? _routeName;
  String? get routeName => _routeName;

  Future<void> _init() async {
    await homeProvider.read.init().then((value) => _routeName = home);
    notify();
  }
}
